#include <iostream>
using namespace std;
#include "Vectori.h"
#include <time.h>

int main() {
	int a[100],n;
	int b[100],m;
	int fibo[100], lungFibo;

	srand(time(NULL));

	Generare1(a, n);
	Afisare(a, n);
	Generare1Recursiv(a, n);
	Afisare(a, n);

	Generare2(b, m);
	Afisare(b, m);
	Generare2Recursiv(b, m);
	Afisare(b, m);

	Generare3(a, n);
	Afisare(a, n);

	Generare4(a, n);
	Afisare(a, n);
	Generare4Recursiv(a, n);
	Afisare(a, n);

	GenerareAiureaCrescator(a, n);
	Afisare(a, n);

	lungFibo = 12;
	GenerareFibo(fibo, lungFibo);
	Afisare(fibo, lungFibo);
}
