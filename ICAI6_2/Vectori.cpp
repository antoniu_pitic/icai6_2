#include "Vectori.h"
#include <iostream>
using namespace std;

void Afisare(int a[], int n)
{
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;

}

void Citire(int a[], int &n)
{
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
}

int DeCateOriAparePrimaValoare(int a[], int n)
{
	int ct = 0;
	for (int i = 0; i < n; i++) {
		if (a[i] == a[0]) {
			ct++;
		}
	}
	return ct;
}

void Generare1(int a[], int &n)
{
	n = 10;
	for (int i = 0; i < n; i++) {
		a[i] = i + 1;
	}
}

void Generare2(int a[], int &n)
{
	n = 10;
	for (int i = 0; i < n; i++) {
		a[i] = n-i;
	}
}

void Generare3(int a[], int &n)
{	
	n = 10;
	for (int i = 0; i < n; i++) {
		a[i] = rand()%10;
	}
}

void Generare4(int a[], int &n)
{
	n = 10;
	for (int i = 0; i < n; i++) {
		a[i] = (i%2==0)?0:i ;
	}
}

void GenerareAiureaCrescator(int a[], int &n)
{
	n = 30;
	for (int i = 0; i < n; i++) {
		a[i] = i*(i/2)-n/2;
	}
}

void Generare1Recursiv(int a[], int &n)
{
	n = 10;
	a[0] = 1;
	for (int i = 1; i < n; i++) {
		a[i] = a[i-1] + 1;
	}
}

void Generare2Recursiv(int a[], int &n)
{	
	n = 10;
	a[0] = n;
	for (int i = 1; i < n; i++) {
		a[i] = a[i-1] - 1;
	}
}

void Generare4Recursiv(int a[], int &n)
{	
	n = 10;
	a[0] = 0;
	a[1] = 1;
	for (int i = 2; i < n; i++) {
		if (i % 2 == 0) {
			a[i] = a[i - 2];
		}
		else {
			a[i] = a[i - 2] + 2;
		}
	}
}

void GenerareFibo(int a[], int &n)
{	
	//n = 10;
	a[0] = 1;
	a[1] = 1;
	for (int i = 2; i < n; i++) {
		a[i] = a[i - 2] + a[i-1];
	}
}
