#pragma once

void Afisare(int[], int);
void Citire(int[], int&);

int DeCateOriAparePrimaValoare(int[], int);

void Generare1(int[], int&);
void Generare2(int[], int&);
void Generare3(int[], int&);
void Generare4(int[], int&);
void GenerareAiureaCrescator(int[], int&);

void Generare1Recursiv(int[], int&);
void Generare2Recursiv(int[], int&);
void Generare4Recursiv(int[], int&);
void GenerareFibo(int[], int&);